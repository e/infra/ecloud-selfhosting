# To upgrade from ecloud 20.x.x.x to 21.0.9.12

- As usual, upgrade your OS with latest patchs, optionally take backup/snapshot

  - NB: you may want to filter out incomming email (TCP 25 & 587) during this upgrade, to avoid losing any messages in case of a rollback

- In your `docker-compose.yml` file update the following:
  - Set the redis image to `redis:6.2-alpine`
  - Set the welcome image to `registry.gitlab.e.foundation/e/infra/docker-welcome:2.5.0`
  - Set the nextcloud image to `registry.gitlab.e.foundation/e/infra/ecloud/nextcloud/selfhost:selfhost-21-0-9-12`
  - Set the nginx image to `nginx:1.20-alpine`
  - Under `welcome`, set the following env variables:
    - ```
      environment:
        ...
        - WELCOME_SMTP_FROM=${SMTP_FROM}
        - WELCOME_SMTP_PW=${SMTP_PW}
        ...
      ```

- Add `ecloud-accounts` secret to secure the `ecloud-accounts` API:
  - Generate a secure secret string
  - Add it to `volumes/nextcloud/html/config/config.php` with an entry like:
    - ```php
      ...
      'ecloud-accounts' => [
        'secret' => 'secure-secret'
      ],
      ...
  - Add this to your `.env` file as `ECLOUD_ACCOUNTS_SECRET=secure-secret`
  - Add it to the environment variables of `welcome` in `docker-compose.yml`:
    - ```
      environment:
        ...
        - ECLOUD_ACCOUNTS_SECRET=${ECLOUD_ACCOUNTS_SECRET}
        ...
      ```
      

- Run `docker-compose pull`
- Run `docker-compose up -d`

- Examine `docker-compose logs --tail=500 nextcloud` for the following messages:
  - `nextcloud       | Upgrading nextcloud from x.x.x.x ...` (x.x.x.x is your previous Nextcloud version)
  - `nextcloud       | Update successful`

- Enable the `ecloud-accounts` app:
  `docker exec -u www-data nextcloud /var/www/html/occ app:enable ecloud-accounts`

- Run `docker exec -T --user www-data nextcloud php occ db:add-missing-indices`
