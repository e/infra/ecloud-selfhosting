# Put files in place
Copy `config/systemd/mariadb*` to `/etc/systemd/system/`

# Add SQL backup user
Generate a password using:
```bash
cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 20 | head -n 1
```
=> the string displayed is to replace $$PASSWORD$$ in following instructions

Edit `/mnt/repo-base/.env`, add:
```
MARIADB_BACKUP_USER=backupuser
MARIADB_BACKUP_PASSWORD=$$PASSWORD$$
```

Edit `/mnt/repo-base/scripts/base.sh`, add:
```
MARIADB_BACKUP_USER=$(grep ^MARIADB_BACKUP_USER= "$ENVFILE" | awk -F= '{ print $NF }')
MARIADB_BACKUP_PASSWORD=$(grep ^MARIADB_BACKUP_PASSWORD= "$ENVFILE" | awk -F= '{ print $NF }')
```

From `/mnt/repo-base/.env`, get value for `MYSQL_ROOT_PASSWORD`  
=> the value is to replace $$ROOTPASSWORD$$ in following instructions  
Issue:
```bash
cd /mnt/repo-base
docker-compose exec -T mariadb mysql --user=root --password="$$ROOTPASSWORD$$" -e "CREATE USER 'backupuser'@'localhost' IDENTIFIED BY '$$PASSWORD$$';"
docker-compose exec -T mariadb mysql --user=root --password="$$ROOTPASSWORD$$" -e "GRANT SELECT, SHOW VIEW, LOCK TABLES, RELOAD, REPLICATION CLIENT ON *.* TO 'backupuser'@'localhost';"
```

# Enable & start backups
Please read `/mnt/repo-base/docs/SQL_backups.md`
