# To upgrade from ecloud 24.0.8.7 to 24.0.10.7

- As usual, upgrade your OS with latest patchs, optionally take backup/snapshot
  - NB: you may want to filter out incomming email (TCP 25 & 587) during this upgrade, to avoid losing any messages in case of a rollback

- Go to `/mnt/repo_base`, then run:
  - `docker-compose stop`
  - `git pull origin master`

- In your `docker-compose.yml` file update the following:
  - Set the mailserver image to `mailserver2/mailserver:1.1.13`
  - In welcome "extra_hosts", add `- "mail.${DOMAIN}:${NC_HOST_IP}"`
  - Set the nextcloud image to `registry.gitlab.e.foundation/e/infra/ecloud/nextcloud/selfhost:selfhost-24-0-10`
  - Set the nginx image to `nginx:stable-alpine`

- Run `crontab -e`, add `--define apc.enable_cli=1` after `"nextcloud php"` for your NextCloud entry
- Run `docker-compose pull`
- If pulls are OK, run `docker-compose up -d`

- Examine `docker-compose logs --tail=500 nextcloud` for the following messages:
  - `nextcloud       | Upgrading nextcloud from x.x.x.x ...` (x.x.x.x is your previous Nextcloud version)
  - `nextcloud       | Update successful`

- Run:
  - `docker-compose exec -T --user www-data nextcloud php occ db:add-missing-indices`
  - `docker-compose exec -T --user www-data nextcloud php /var/www/html/occ config:system:set defaultapp --value "ecloud-dashboard,files"`

- Snappymail is being intorduced in this version, so make sure that rainloop is disabled. 
  - Run `docker-compose exec -T --user www-data nextcloud php occ app:disable rainloop`
  - Enable snappymail `docker-compose exec -T --user www-data nextcloud php occ app:enable snappymail` (you can pass --groups gid to limit access to specific group for testing)
  - Set auto-login with `docker-compose exec -T --user www-data nextcloud php /var/www/html/occ config:app:set snappymail snappymail-autologin-with-email --value 1`
  - Import user data from rainloop to snappymail
    - Go to settings > admin > additional and check `Import RainLoop data` and hit save. This will import all user data/settings from rainloop to snappymail
  - Now go to snappymail admin panel
    - Make sure to change default password
    - Set theme to Murena@nextcloud
    - Disable all domains except yours
    - In your domain settings, disable "Require verification of SSL certificate" in all tabs 
    - Make changes to other settings where needed and click save
    - If contacts address book was enabled in rainloop, you can use the same database for contacts in snappymail
    - Go to extensions section then install nextcloud plugin and enable it

  - Advise users that they should review SnappyMail settings:
    - Default theme (to be set to Murena)
    - Contacts (copy/paste URL from Contacts app, enable)
    - Filters

- Check all settings subsections starting from `/settings/admin/overview` while logged in with the admin user account to identify any issues found post upgrade