# To upgrade from ecloud 23.x.x.x to 24.0.8.7

- As usual, upgrade your OS with latest patchs, optionally take backup/snapshot
  - NB: you may want to filter out incomming email (TCP 25 & 587) during this upgrade, to avoid losing any messages in case of a rollback

- Go to `/mnt/repo_base`, then run:
  - `docker-compose stop`
  - `git pull origin master`

- In your `docker-compose.yml` file update the following:
  - Set the nextcloud image to `registry.gitlab.e.foundation/e/infra/ecloud/nextcloud/selfhost:24-0-8-7`
  - Set the mailserver image to `mailserver2/mailserver:1.1.12`

- Run `docker-compose pull`
- If pulls are OK, run `docker-compose up -d`

- Examine `docker-compose logs --tail=500 nextcloud` for the following messages:
  - `nextcloud       | Upgrading nextcloud from x.x.x.x ...` (x.x.x.x is your previous Nextcloud version)
  - `nextcloud       | Update successful`

- Run:
  - `docker-compose exec -T --user www-data nextcloud php occ db:add-missing-indices`

- Check all settings subsections starting from `/settings/admin/overview` while logged in with the admin user account to identify any issues found post upgrade