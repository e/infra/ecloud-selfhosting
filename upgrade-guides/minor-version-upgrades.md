# To perform a minor version upgrade(version 24.0.8 onwards)

- Check the [releases](https://gitlab.e.foundation/e/infra/ecloud/nextcloud/-/releases/) page for a new minor version release
- In your `docker-compose.yml` file update the following:
  - Set the nextcloud image to the new tag as found from the releases page
    - For example, if the release is "24.0.9.1", the image entry should look like:
      ```yaml
      image: registry.gitlab.e.foundation/e/infra/ecloud/nextcloud/selfhost:24-0-9-1
      ```

- Run `docker-compose stop nextcloud`
- Run `docker-compose pull nextcloud`
- If pulls are OK, run `docker-compose up -d nextcloud`
- Examine `docker-compose logs --tail=500 nextcloud` for the following messages:
  - `nextcloud       | Upgrading nextcloud from x.x.x.x ...` (x.x.x.x is your previous Nextcloud version)
  - `nextcloud       | Update successful`


- Check all settings subsections starting from `/settings/admin/overview` while logged in with the admin user account to identify any issues found post upgrade