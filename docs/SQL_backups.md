# Review configuration
- Check and adjust to your needs `KEEP` and `BACKUPS_PATH` in `/mnt/repo-base/scripts/mariadb-nc-backup.sh` and `/mnt/repo-base/scripts/mariadb-nc-backup.sh`
    - `KEEP`: number of files to be kept at end of backup
    - `BACKUPS_PATH`: backups storage (can be a mount point, i.e. a Hetzner Volume)
- Check startup time (`OnCalendar`) in `/etc/systemd/system/mariadb-nc-backup.timer` and `/etc/systemd/system/mariadb-pf-backup.timer`
    - Reference: <https://www.freedesktop.org/software/systemd/man/systemd.time.html#Calendar%20Events>

# Activate scheduled backups
- If you changed startup time in timer units, issue a:
```bash
 systemctl daemon-reload
 ```
 - Then check with:
```bash
 systemctl list-timers
 ```
 - Hint: if your timer are not listed, issue these commands:
```bash
 systemctl enable mariadb-nc-backup.timer
 systemctl enable mariadb-pf-backup.timer
```
- Finaly, start timers with:
```bash
 systemctl start mariadb-nc-backup.timer
 systemctl start mariadb-pf-backup.timer
```
- Then check with with:
```bash
 systemctl status mariadb-nc-backup.timer
 systemctl status mariadb-pf-backup.timer
```

# Review execution log
After execution (probably the day after), you can check with:
```bash
 systemctl status mariadb-nc-backup.timer
 systemctl status mariadb-pf-backup.timer
 systemctl status mariadb-nc-backup.service
 systemctl status mariadb-pf-backup.service
```
And:
```bash
journalctl -u mariadb-*-backup.service
```

